﻿using System;

namespace AbstractFactory
{
    public class Laptop : IPrint
    {
        public string Brand { get;}
        public int CountCore { get; }
        public int RAM { get; }
        public string DriveType {  get; }
        public string VideoCard { get; }
        public string Processor { get; }

        public Laptop(string brand,int countCore, int ram, string driveType, string videoCard, string processor)
        {
            Brand = brand;
            CountCore = countCore;
            RAM = ram;
            DriveType = driveType;
            VideoCard = videoCard;
            Processor = processor;
        }
        public string Print()
        {
            return $"Brand: {Brand}, CountCore: {CountCore}, RAM: {RAM}GB, DriveType: {DriveType}, VideoCard: {VideoCard}, Processor: {Processor}";
        }

    }
}

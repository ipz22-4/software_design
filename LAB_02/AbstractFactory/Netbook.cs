﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    public class Netbook : IPrint
    {
        public string Brand { get; }
        public int CountCore { get; }
        public int RAM { get; }
        public string DriveType { get; }
        public string Processor { get; }

        public Netbook(string brand,int countCore, int ram, string driveType, string processor)
        {
            Brand = brand;
            CountCore = countCore;
            RAM = ram;
            DriveType = driveType;
            Processor = processor;
        }
        public string Print()
        {
            return $"Brand: {Brand}, CountCore: {CountCore}, RAM: {RAM}GB, DriveType: {DriveType}, Processor: {Processor}";
        }
    }
}

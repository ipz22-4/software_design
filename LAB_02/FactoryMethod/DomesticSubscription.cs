﻿
namespace FactoryMethod
{
    public class DomesticSubscription : Subscription
    {
        public DomesticSubscription(List<string> channels) : base(99.99, 6, channels)
        {

        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryToShop
{
    public interface IMoney
    {
        public int FullPrice { get; set; }
        public int Partprice { get; set; }
        string Print();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryToShop
{
    public class Euro : IMoney
    {
        public int FullPrice { get; set; }
        public int Partprice { get; set; }

        public Euro(int euros, int eurocent)
        {
            FullPrice = euros;
            Partprice = eurocent;
        }
        public string Print()
        {
            return $"{FullPrice}.{Partprice} €";
        }
    }
}

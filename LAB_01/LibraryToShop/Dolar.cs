﻿using System.Drawing;

namespace LibraryToShop
{
    public class Dolar : IMoney
    {
        public int FullPrice { get; set; }
        public int Partprice { get; set; }

        public Dolar(int dolar, int cent)
        {
            FullPrice = dolar;
            Partprice = cent;
        }

        public string Print()
        {
            return $"{FullPrice}.{Partprice} $";
        }
    }

}

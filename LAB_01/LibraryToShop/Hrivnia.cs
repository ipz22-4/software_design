﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryToShop
{
    public class Hrivnia : IMoney
    {
        public int FullPrice { get; set; }
        public int Partprice { get; set; }

        public Hrivnia(int hrivni, int kopeck)
        {
            FullPrice = hrivni;
            Partprice = kopeck;
        }
        public string Print()
        {
            return $"{FullPrice} грн.{Partprice} копійок";
        }
    }
}

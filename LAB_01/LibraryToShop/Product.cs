﻿using System;

namespace LibraryToShop
{
    public class Product : IAmount
    {
        public string Name { get; set; }
        public IMoney Price { get; set; }
        public int Count { get; set; }
        public string Category { get; set; }


        public Product(string name, IMoney money, int count,string category)
        {
            Name = name;
            Price = money;
            Count = count;
            Category = category;
        }

       
        public void DecreasePrice(IMoney amount)
        {
            if (amount.FullPrice <= 0)
            {
                Console.WriteLine("Знижка не має бути не від'ємною.");
            }
            else if (amount.FullPrice >= Price.FullPrice)
            {
                Console.WriteLine("Знижка має бути меншою за ціну.");
            }
            else
            {
                Price.FullPrice -= amount.FullPrice;
                Console.WriteLine($"Ціна товару {Name} зменшилася на {amount.FullPrice}. Нова ціна: {Price.FullPrice}");
            }
        }
    }
}

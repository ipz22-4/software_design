﻿using LibraryToShop;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shop
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Warehouse warehouse = new Warehouse();
            List<Product> products = new List<Product>();

            Product product1 = new Product("Триста поезій", new Dolar(10, 97), 10, "Книги");
            Product product2 = new Product("Маруся-Чурай", new Hrivnia(320, 97), 15, "Книги");
            Product product3 = new Product("Ноутбук Lenovo IdeaPad Gaming 3 15ACH6", new Euro(100, 0), 5, "Ноутбук");
            Console.WriteLine("Усі продукти:");
            Console.WriteLine($"Триста поезій - Книги  - {product1.Price.Print()}");
            Console.WriteLine($"Маруся-Чурай - Книги  - {product2.Price.Print()}");
            Console.WriteLine($"Ноутбук Lenovo IdeaPad Gaming 3 15ACH6 - Ноутбук  - {product3.Price.Print()}");


            warehouse.AddProduct(product1, "кг", product1.Count, DateTime.Now);
            warehouse.AddProduct(product2, "кг", product2.Count, DateTime.Now);
            Console.WriteLine();
            Console.WriteLine("Знижка на товар Триста поезій");
            product1.DecreasePrice(new Dolar(5, 0));
            warehouse.RemoveProduct("Триста поезій", 5);

            Console.WriteLine();
            Console.WriteLine("Усі продукти на складі:");
            ConsoleReport consoleReport = new ConsoleReport();
            consoleReport.GenerateReport(warehouse);
            Console.WriteLine();
            Console.WriteLine("Виведення товарів по категорії:");
            List<Product> productsByCategory = warehouse.GetProductsByCategory("Книги");
            foreach (var product in productsByCategory)
            {
                Console.WriteLine($"{product.Name} - {product.Count} шт {product.Price.Print()}");
            }
        }

    }
}
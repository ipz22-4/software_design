﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoRClassLibrary
{
    public class FifthLevel : BaseHandler
    {
        protected override string Report => "Check your credentials or try a different network";

        protected override int Level => 5;

        protected override string Problem => "The device cannot join the network";

        public override void Request(UserRequest request)
        {
            if (request.UserProblem.Equals(Problem, StringComparison.CurrentCultureIgnoreCase))
            {
                Console.WriteLine($"Level {Level} support: {this.Report}");
                return;
            }

            base.Request(request);
        }
    }
}

﻿using BehavioralClassLibrary;

namespace LAB_05
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var divElement = new LightElementNode("body", "block", "double", new List<string>(), new List<LightNode>
            {
                 new LightElementNode("div", "block", "single", new List<string>{"messange"}, new List<LightNode>
                 {
                    new LightTextNode("Hello everyone, it's me "),
                    new LightElementNode("span", "inline", "single", new List<string>{"name"}, new List<LightNode>
                    {
                        new LightTextNode("Shadow!")
                    })
                 }),
                 new LightElementNode("p", "block", "double", new List<string>(), new List<LightNode>
                 {
                     new LightTextNode("Some text ")
                 })
            });

            string html = divElement.OuterHTML;
            string node = divElement.InnerHTML;

            Console.WriteLine("Generated HTML element:");
            Console.WriteLine(html);
            Console.WriteLine();
            Console.WriteLine("LightTextNode text:");
            Console.WriteLine(node);
        }
    }
}
